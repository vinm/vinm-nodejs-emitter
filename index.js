module.exports = function () {
    return function (name, data) {
        if (Array.isArray(data)) data = Object.assign({}, data)
        const vinmData = JSON.stringify(
            { name: name, value: JSON.stringify(data) }
        )
        process.stdout.write(`vinm.stdout:${vinmData}`)
    }
}