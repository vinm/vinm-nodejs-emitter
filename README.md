# Vīnm Node.js Emitter

> Node.js emitter for Vinm CLI.

🚸 Extend the [Vinm CLI](https://gitlab.com/vinm/vinm-cli)

When executing a Node.js script using Vinm CLI, vinm-emitter provide the ability to emit variables back to the execution pipeline so that it can be used in all the following tasks.


## 📦 Installation

```sh
# With npm
npm i vinm-nodejs-emitter

# OR With yarn
yarn add vinm-nodejs-emitter
```


## 🚀 Usage

Considering the below Node.js script `myscript.js`:

```javascript
module.exports.getSecret = () => {
    const vinmEmit = require('vinm-nodejs-emitter')()
    vinmEmit('mysecret', '123456')
}
```

Here's how we would access the `mysecret` var from `vinm.yml`:

```yml
tasks:
    get-mysecret:
        shell: >-
            node -e "require(\"./myscript.js\").getSecret()"
    print-mysecret:
        shell: >-
            echo "$vinm.mysecret"
```


## 🤝 Contributing

Contributions, issues and feature requests are welcome.


## 📝 License

Copyright © 2020 [maoosi](https://gitlab.com/maoosi).<br />
This project is [MIT](./LICENSE) licensed.
